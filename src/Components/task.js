import React from 'react';

import './task.css';


const Task = (props) => {
    return (
        props.tasks.map((task, index) => {
            return (
                <div key={index} className="task">
                    <h1>
                        {task.text}
                    </h1>
                    <button className="btn" onClick={() => props.remove(task.id)}>delete</button>
                </div>
            )
        })
    )
};

export default Task;