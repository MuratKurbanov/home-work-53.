import React from 'react';

const AddTaskForm = (props) => {
    return (
        <div>
            <input value={props.value} type="text" placeholder="Add new task" onChange={props.changed}/>
            <button onClick={props.add}>Add</button>
        </div>
    )
};

export default AddTaskForm;