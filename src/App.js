import React, { Component,} from 'react';
import TaskForm from './Components/AddTaskForm';

import './App.css';
import Task from "./Components/task";

class App extends Component {
  state = {
      text: '',
      tasks: [
          {text: 'Купить', id: 0},
          {text: 'Продать', id: 1},
          {text: 'Взять', id: 2},
      ],
      currentId: 3
  };

  add = () => {
      let tasks = [...this.state.tasks];
      const newTask = {text: this.state.text, id: this.state.currentId + 1};
      tasks.push(newTask);
    this.setState({
        tasks: tasks
    });
  };

  changeHandler = (event) => {
      this.setState({text: event.target.value})
  };

  remove = (id) => {
      let remove = [...this.state.tasks];
      const index = remove.findIndex(task => task.id === id)
      remove.splice(index, 1);
      this.setState({tasks: remove})
  };

  render() {
    return (
      <div className="App">
        <TaskForm add={() => this.add()} value={this.state.text} changed={(event) => this.changeHandler(event)}/>
          <Task remove={(id) => this.remove(id)} tasks={this.state.tasks}/>
      </div>
    );
  }
}

export default App;
